from dasbus.connection import SessionMessageBus
import time
bus = SessionMessageBus()

proxy = bus.get_proxy(
    "org.freedesktop.Notifications",
    "/org/freedesktop/Notifications"
)

id = proxy.Notify(
    "", 0, "face-smile", "Hello World!",
    "This notification can be ignored.",
    [], {}, 0
)

time.sleep(2)

id2 = proxy.CloseNotification(id)

print("The notification {} was sent.".format(id))
# Call the Introspect method of the remote object.
# print(proxy.Introspect())

# busproxy = bus.proxy
# for i in sorted(busproxy.ListNames()):
#     print(i)