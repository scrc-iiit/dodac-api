# Dodac API to Send Wi-SUN Status from Border Router

This project provides a RESTful API to send Wi-SUN status from a border router. The API is built using **FastAPI** and deployed using **Gunicorn Runtime**. It exposes endpoints that interact with the border router to retrieve Wi-SUN status using the `wsbrd-cli` tool.

## Table of Contents
- [Introduction](#introduction)
- [Setup and Run](#setup-and-run)
  - [Configuration](#configuration)
- [Shutting Down Gunicorn](#shutting-down-gunicorn)
- [Maintainers](#maintainers)

## Introduction
The Dodac API interacts with a Wi-SUN border router to fetch its connection status. The API is built with **FastAPI** for fast, asynchronous web services and supports both **Swagger** and **Redoc** for API documentation.

- **Swagger Docs**: [http://localhost:8000/docs](http://localhost:8000/docs)
- **Redoc Docs**: [http://localhost:8000/redoc](http://localhost:8000/redoc)

## Setup and Run

### 1. Clone the Repository
Clone the repository to your local machine:
```bash
git clone https://gitlab.com/scrc-iiit/dodac-api.git
cd dodac-api
```

### 2. Set Up the Virtual Environment
Create and activate a Python virtual environment:
```bash
python -m venv venv
source venv/bin/activate   # On Linux/macOS
.\venv\Scripts\activate    # On Windows
```

### 3. Install Dependencies
Install all required Python packages from `requirements.txt`:
```bash
pip install -r requirements.txt
```

### Configuration
- Change the Number of Worker Threads, Port etc using [gunicorn.conf.py](gunicorn.conf.py)



### 4. Run the Application
You can run the application using Gunicorn for production. To start the server, use the following command:
```bash
gunicorn -c gunicorn_conf.py &
```

## Shutting Down Gunicorn 
If you need to stop the Gunicorn service but don't know the process ID, you can use the following command to kill the process running on port 8000:
```bash
   lsof -t -i:8000 | xargs kill 
```
## Maintainers
Current maintainer of the project:
- **Surya Suhaas Modadugu** [(mssuhaas)](mailto:mssuhaas@gmail.com)

Former maintainer:
- **Leo Jesvyn Francis** [(leojfrancis)](mailto:leojfrancis.now@gmail.com)
