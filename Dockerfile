# syntax=docker/dockerfile:1

FROM python:3.10.5

LABEL maintainer="leojfrancis.now@gmail.com"
LABEL description="API for Dodac structure"

ENV PIP_DISABLE_PIP_VERSION_CHECK 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /workspace
COPY requirements.txt .
RUN pip install --no-cache-dir --upgrade -r requirements.txt

COPY gunicorn.conf.py .
COPY ./scripts ./scripts
RUN chmod +x ./scripts/entrypoint.sh

ARG RE_BUILD=unknown
COPY ./app ./app

EXPOSE 8000

CMD [ "sh","./scripts/entrypoint.sh" ]
