#!/bin/bash

echo "---Updating machine---"
sudo apt-get update && sudo apt-get upgrade

echo "---Installing Docker---"
curl -fsSL https://get.docker.com -o get-docker.sh

sudo groupadd docker

sudo usermod -aG docker $USER

newgrp docker

docker run hello-world

sudo systemctl enable docker.service
sudo systemctl enable containerd.service