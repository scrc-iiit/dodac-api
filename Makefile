run_file:
	python app/command_parser.py
run:
	uvicorn app.main:app --reload
gunicorn:
	gunicorn
up:
	docker-compose -f internal-deployment.yml up -d
