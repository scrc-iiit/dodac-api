import subprocess
import logging
import typing
from fastapi import FastAPI, Request, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from . import command_parser
import time
from pydantic import BaseModel, BaseSettings


class WisunStatus(BaseModel):
    network_name: str
    domain: str
    mode: str
    _class: str 
    panid: str 
    size: str 
    tree: typing.List[typing.Tuple[str, str]]


COMMAND = "wsbrd_cli status"
logging.basicConfig(level=logging.DEBUG,
                    format='[%(asctime)s] [%(process)d] [%(levelname)s] %(message)s')



class Settings(BaseSettings):
    app_name: str = "Dodac API"
    allowed_hosts: list = ['*']
    root_path: str = ""

    class Config:
        env_file = ".env"

settings = Settings()
app = FastAPI(root_path=settings.root_path)

print(settings)

app.add_middleware(
    CORSMiddleware,
    allow_origins=settings.allowed_hosts,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    response.headers["X-Process-Time"] = str(process_time)
    return response

@app.get("/")
async def get_dodac_info():
    process = subprocess.run(COMMAND, shell=True, capture_output=True)
    if process.stderr:
        err = process.stderr.decode().replace("\n", "")
        logging.warning(f'{err}')
        raise HTTPException(status_code=500, detail=f"{err}")
    output = process.stdout.decode()
    try:
        return command_parser.get_dodac_properties(output)
    except Exception as err:
        logging.warning(f'{err}')
        raise HTTPException(status_code=500, detail=f"{err}")


@app.get("/sample")
async def get_dodac_info_testing():
    output = command_parser.SAMPLE_OUTPUT_STRING
    try:
        return command_parser.get_dodac_properties(output)
    except Exception as err:
        logging.warning(f'{err}')
        raise HTTPException(status_code=500, detail=f"{err}")
