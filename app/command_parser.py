import re
from pprint import pprint

# Helper Functions
SAMPLE_OUTPUT_STRING = """
network_name: Wi-SUN Network
domain: IN
mode: 1a
class: 1
panid: 0xdedd
size: SMALL
GAK[0]: b3:c0:0a:da:c2:5c:f3:25:db:5e:4a:77:eb:41:e3:a7
GAK[1]: 26:1b:ac:fe:97:e5:2e:f1:3a:e5:d9:af:38:c2:0f:f3
GAK[2]: 26:1b:ac:fe:97:e5:2e:f1:3a:e5:d9:af:38:c2:0f:f3
GAK[3]: 26:1b:ac:fe:97:e5:2e:f1:3a:e5:d9:af:38:c2:0f:f3
GTK[0]: 2a:06:2a:df:1f:27:37:c1:1a:56:77:b4:12:ab:bf:29
GTK[1]: 00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
GTK[2]: 00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
GTK[3]: 00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
LGAK[0]: 39:b0:cd:46:a2:e2:cd:51:87:ce:49:7b:fe:38:3c:ff
LGAK[1]: 4a:ed:e0:13:af:df:5e:9b:c5:c7:03:f1:fe:c5:f9:52
LGAK[2]: 4a:ed:e0:13:af:df:5e:9b:c5:c7:03:f1:fe:c5:f9:52
LGTK[0]: a1:d9:2d:65:bb:c2:48:0d:a6:2a:c6:0c:f3:b4:ca:a1
LGTK[1]: 00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
LGTK[2]: 00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
fd12:3456::92fd:9fff:feee:9d4d
  |- fd12:3456::eae:5fff:fe6d:4fe1
  |    `- fd12:3456::b635:22ff:fe98:253c
  `- fd12:3456::b635:22ff:fe98:29a7
"""


def generate_pattern(string): 
    return f"(?<={string}: ).*"

def get_ipv6(string):
    ipv6_pattern = r'(?:[0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:)*(?:::)?(?:[0-9a-fA-F]{1,4}:)*[0-9a-fA-F]{1,4}(?:::)?(?:[0-9a-fA-F]{1,4}:)*(?:[0-9a-fA-F]{1,4})?'
    match = re.search(ipv6_pattern, string)
    if match:
        return match.group(0)
    return None

def get_properties(input_string: str) -> dict:
    # can optimize with groups
    key = re.findall("(\w+)\: ", input_string)
    value = re.findall(generate_pattern("\w"), input_string)
    return dict(zip(key, value))


def get_groups(input_string: str) -> list:
    # can optimize with groups
    key = re.findall("\S{3}\[\d\]", input_string)
    value = re.findall(generate_pattern("\S{3}\[\d\]"), input_string)
    return list(zip(key, value))


def get_tree(input_string: str):
    groups = get_groups(input_string)
    if not groups:
        raise Exception("Cannot get groups")
    
    top, bottom = input_string.split(f"{groups[-1][0]}: {groups[-1][1]}\n")
    bottom_lines = bottom.splitlines()
    border_router = bottom_lines.pop(0)
    parent_list = []
    tree = []
    prev_len = 0
    prev_node = border_router
    parent_list = [border_router]  
    for line in bottom_lines:
        if not line.strip(): 
            continue
        current_len = len(re.findall("[|]|[`]|[ ]", line))
        current_node = get_ipv6(line)
        if not current_node:  
            continue
        
        while len(parent_list) > current_len // 4:
            parent_list.pop()
            
        if parent_list:
            tree.append([parent_list[-1], current_node])
        if '`-' not in line:
            parent_list.append(current_node)    
        prev_len = current_len
        prev_node = current_node
    return tree

def get_dodac_properties(output_string:str) -> dict:
    meta_data = get_properties(output_string)
    tree = get_tree(output_string)
    return {**meta_data, "tree": tree}

if __name__ == "__main__":
    pprint(get_dodac_properties(SAMPLE_OUTPUT_STRING))